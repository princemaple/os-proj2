all: main.o pop.o socket.o maildir.o logging.o
	gcc -o pop3 main.o pop.o socket.o maildir.o logging.o -lpthread -lrt -lnsl
pop3: all

main.o: main.c pop.h socket.h maildir.h logging.h
	gcc -c -lpthread -lrt -lnsl main.c

pop.o: pop.c pop.h socket.h maildir.h logging.h
	gcc -c -lpthread -lrt -lnsl pop.c

socket.o: socket.c socket.h
	gcc -c -lpthread -lrt -lnsl socket.c

maildir.o:  maildir.c maildir.h logging.h
	gcc -c -lpthread -lrt -lnsl maildir.c

logging.o: logging.c logging.h
	gcc -c -lpthread -lrt -lnsl logging.c

clean:
	rm *.o pop3
