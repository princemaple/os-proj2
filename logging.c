#include <pthread.h>
#include <stdio.h>
#include <time.h>

#include "logging.h"

FILE *logFile;
time_t currentTime;
char *timeString;

void chopNewline( char * );

/*
* This function is to be called once,
* before the first call to log_write is made.
*
* log_file_name is the path and file name of the file to be used for logging.
*/
void log_setUp( char* logFileName ){
	logFile = fopen( logFileName, "a" );
}

/*
* Writes a log entry to the log file.
* entries must be formatted with this pattern: "%s: %s: %s: %s: %s\n"
* From left to right the %s are to be:
*  - a time stamp, in local time.
*  - module (which part of the program created the entry?)
*  - p1
*  - p2
*  - p3
*
* Note: This function may be called from multiple threads and hence must
*       ensure that only one thread is writing to the file at any given time.
*/

static pthread_mutex_t loggingMutex = PTHREAD_MUTEX_INITIALIZER;

void log_write( char* module, char* p1, char* p2, char* p3 ){
	pthread_mutex_lock( &loggingMutex );
    /* Obtain current time as seconds elapsed since the Epoch. */
    currentTime = time( NULL );
    if ( currentTime == ( ( time_t )-1 ) ){
        /* Will this ever fail? */
        fprintf( stderr, "Failure to compute the current time." );
    }
    /* Convert to local time format. */
    timeString = ctime( &currentTime );
    /* chop the unwanted newline */
    chopNewline(timeString);
    if ( timeString == NULL ){
        /* Will this ever fail?? */
        fprintf( stderr, "Failure to convert the current time." );
    }
	fprintf( logFile, "%s: %s: %s: %s: %s\n", timeString, module, p1, p2, p3 );
	fflush( logFile );
	printf( "%s: %s: %s: %s: %s\n", timeString, module, p1, p2, p3 );
	pthread_mutex_unlock( &loggingMutex );
}

/* utility function! give me header file access!! */
void chopNewline( char *line ){
    int i = 0;
    while( line[i] != '\n' && line[i] != '\r' && line[i] != '\0' ){
    	i++;
    }
	line[i] = '\0';
}
