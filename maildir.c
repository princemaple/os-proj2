#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "logging.h"
#include "maildir.h"

#define BUFLEN 512
#define LOG_MAILDIR "MailDir"
#define GET_LIST "GetList"
#define GET_FILE "GetFile"
#define DELETE "Delete"

#define ERROR_DELETE "ERROR deleting file"

/* from main.c */
extern char *EMPTY;

static char *MAILDIR = NULL;

typedef struct user_list_t UserList;

struct user_list_t{
	char *username;
	char *password;
	UserList *next;
};

static UserList dummy = { NULL, NULL, NULL };

static void setPassword( char **, char *, char * );
static int countFiles( char * );

/* to eliminate warnings, I don't know why readdir_r is `implicitly` declare */
/* is it not included in dirent.h ??? */
int readdir_r( DIR *, struct dirent *, struct dirent ** );

/* from logging.c, we are not allowed to modify header files, so.. */
void chopNewline( char * );

/* from main.c */
void * smalloc( size_t );

/*
* This function is to be called once at the beginning
* to set the root path of the maildirs.
*
* It also stores some user & password information in the memory
*/
void dir_set_path( char* path ){
	MAILDIR = path;

	struct dirent *direntp;
	DIR *dirp;
	UserList *ul = &dummy;
	int len;

	if( ( dirp = opendir( MAILDIR ) ) == NULL ) {
		fprintf( stderr, "Failed to open directory" );
		exit( 1 );
	}

	while( ( direntp = readdir( dirp ) ) != NULL ) {
		if( !strcmp( direntp->d_name, "." ) ||
				!strcmp( direntp->d_name, ".." ) ){
			continue;
		}

		/* building a user info list */
		ul->next = smalloc( sizeof( UserList ) );
		if( ul->next == NULL ){
			fprintf( stderr, "Error allocating memory. Abort.\n" );
			exit( 1 );
		}

		/* set username and password */
		len = strlen( direntp->d_name );
		ul->next->username = smalloc( sizeof( char ) * len );
		strcpy( ul->next->username, direntp->d_name );
		ul->next->password = NULL;
		setPassword( &ul->next->password, ul->next->username, path );

		/* next user */
		ul->next->next = NULL;
		ul = ul->next;
	}

	// testing();
	closedir( dirp );
}

/*
* This function checks the given username and password
* against the stored values.
*
* If the username is valid (has a corresponding folder)
* and the given password matches the pass file
* then the function should return 'true'.
*
* Otherwise it is to return 'false'.
*
* Note 'true' and 'false' are defined in maildir.h
*/
bool check_user( char* username, char* password ){
	UserList *ul = &dummy;
	/* go through the user info list */
	/* stop on the correct user, or the end of the list*/
	while( ul->next && strcmp( username, ul->next->username ) ){
		ul = ul->next;
	}
	if( ul->next && !strcmp( ul->next->username, username )
		&& !strcmp( ul->next->password, password ) ){
		return true;
	}else{
		return false;
	}
}

static pthread_mutex_t IOMutex = PTHREAD_MUTEX_INITIALIZER;
/*
* Constructs a FilesStruct as described in maildir.h
* for the maildir of the given user.
*/
FilesStruct* dir_get_list( char* user ){
	/* readdir_r not working on uni server */
	/* a lock is used to make the use of readdir safe */
	pthread_mutex_lock( &IOMutex );

	struct dirent *temp;
	struct stat fileStat;
	DIR *dirp;
	char buffer[BUFLEN];
	FilesStruct *files = smalloc( sizeof( FilesStruct ) );

	sprintf( buffer, "%s/%s", MAILDIR, user );

	files->count = countFiles( buffer );
	files->FileSize = smalloc( sizeof( int ) * files->count );
	files->FileNames = smalloc( sizeof( char * ) * files->count );

	if( ( dirp = opendir( buffer ) ) == NULL ) {
		/* this is not expected to fail */
		/* because in order to log on, a user folder has to exist */
		fprintf( stderr, "Failed to open user directory" );
		exit( 1 );
	}

	int count = 0;

	while( ( temp = readdir( dirp ) ) != NULL && count < files->count ){
		if( !strcmp( temp->d_name, "." ) || !strcmp( temp->d_name, ".." )
			|| !strcmp( temp->d_name, "pass" ) ){
			continue; // not interested in "." and ".."
		}
		sprintf( buffer, "%s/%s/%s", MAILDIR, user, temp->d_name );
		if( stat( buffer, &fileStat ) < 0 ){
			fprintf( stderr, "Error reading file %s\n", buffer );
			exit( 1 );
		}
		files->FileNames[count] = smalloc( sizeof( char )
			* strlen( temp->d_name ) );
		strcpy( files->FileNames[count], temp->d_name );
		files->FileSize[count] = fileStat.st_size;
		count++;
	}

	closedir( dirp );

	pthread_mutex_unlock( &IOMutex );

	log_write( LOG_MAILDIR, GET_LIST, user, EMPTY );
    return files;
}

/*
* Delete the given file from the maildir of the given user.
*/
void delete_mail( char* user, char* filename ){
	char *buffer = smalloc( sizeof( char ) * BUFLEN );
	log_write( LOG_MAILDIR, user, DELETE, filename );
	sprintf( buffer, "%s/%s/%s", MAILDIR, user, filename );
	/* we are not allowed to change header files */
	/* and this functions does not know the fd */
	/* and this function returns void */
	/* if anything goes wrong, bad luck. nothing can be done */
	if( remove( buffer ) ){
		/* let's kindly write in the log to record the error */
		log_write( LOG_MAILDIR, user, ERROR_DELETE, EMPTY );
	}
	free( buffer );
}

/*
* Returns the contents of a given file from a user's maildir.
*
* Byte-Stuffing is done else where
*/
char* get_file( char* user, char* filename ){
	pthread_mutex_lock( &IOMutex );
	char *buffer = smalloc( sizeof( char ) * BUFLEN ), *file;
	int size;
	FILE *f;

	sprintf( buffer, "%s/%s/%s", MAILDIR, user, filename );
	f = fopen( buffer, "r" );

	fseek( f, 0, SEEK_END );
	size = ftell( f );
	fseek( f, 0, SEEK_SET );

	file = smalloc( sizeof( char ) * size + 1 );
	fread( file, sizeof( char ), size, f );
	file[size] = '\0';

	fclose( f );

	free( buffer );
	pthread_mutex_unlock( &IOMutex );
	log_write( LOG_MAILDIR, user, GET_FILE, filename );
	return file;
}

int countFiles( char *path ){
	struct dirent *temp;
	DIR *dirp;
	int counter = 0;

	if( ( dirp = opendir( path ) ) == NULL ) {
		/* this is not expected to fail */
		/* because in order to log on, a user folder has to exist */
		fprintf( stderr, "Failed to open user directory" );
		exit( 1 );
	}

	while( ( temp = readdir( dirp ) ) != NULL ) {
		if( strcmp( temp->d_name, "." ) && strcmp( temp->d_name, ".." )
			&& strcmp( temp->d_name, "pass" ) ){
			counter++;
		}
	}

	closedir( dirp );

	return counter;
}

void setPassword( char **password, char *username, char *path ){
	FILE *pass;
	int passLen;

	/* set password path */
	char *buffer = smalloc( sizeof( char ) * BUFLEN );
	sprintf( buffer, "%s/%s/pass", path, username );

	/* open file, get password and close file */
	pass = fopen( buffer, "r" );
	if( pass == NULL ){
		fprintf( stderr, "Error reading password\n" );
		exit( 1 );
	}
	fgets( buffer, BUFLEN, pass );
	fclose( pass );

	/* get the length of the password for allocating memory */
	passLen = strlen( buffer );
	*password = smalloc( sizeof( char ) * ( passLen ) );
	/* set the password, then done */
	strcpy( *password, buffer );
	/* clean the \n in the password */
	chopNewline( *password );
	free( buffer );
}
