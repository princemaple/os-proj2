#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "logging.h"
#include "maildir.h"
#include "pop.h"
#include "socket.h"

#define MAXCON 100

char *EMPTY = "\0";
static char *LOG_MAIN = "Main";
static char *STARTING = "POP3 Server Started";
static char *NEW_CONNECTION = "New connection";
static char *MAX_CON_LIMIT = "Maximum connections present, let new connections wait";

typedef struct threadRecord TR;

struct threadRecord{
	pthread_t thread;
	bool used;
	int portNo;
};

static TR clientHandler[MAXCON];
static pthread_mutex_t threadMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t mallocMutex = PTHREAD_MUTEX_INITIALIZER;

static void cleanupHandler( void * );
static void *threadHandler( void * );
static int getFirstUnusedRecord( TR * );
void * smalloc( size_t size );

int main( int args, char *argv[] ){

	// signal( SIGINT, SIG_IGN );
	// signal( SIGQUIT, SIG_IGN );
	// signal( SIGTSTP, SIG_IGN );
	// signal( SIGKILL, SIG_IGN );

	if( args != 4 ){
		printf( "USAGE: pop3 [port] [maildir path] [log file]\n" );
		printf( "NOTE: the maildir path must end with an '/'\n" );
		exit( 1 );
	}

	char *mailPath, *logPath;
	int portNo, listenFD;

	portNo = atoi( argv[1] );
	mailPath = argv[2];
	logPath = argv[3];

	/* set up every thing */
	log_setUp( logPath );
	dir_set_path( mailPath );
	listenFD = socket_setup( portNo );

	/* Server ready */
	log_write( LOG_MAIN, STARTING, EMPTY, EMPTY );

	int talkPort, i;

	for( i = 0; i < MAXCON; i++ ){
		clientHandler[i].used = false;
		clientHandler[i].portNo = 0;
	}

	while( true ){
		/* permanent loop for accepting new connections */
		talkPort = socket_get_new_connection( listenFD );

		pthread_mutex_lock( &threadMutex );
		i = getFirstUnusedRecord( clientHandler );
		pthread_mutex_unlock( &threadMutex );

		if( i < 0 ){ // I guess this part is not required by the project
			log_write( LOG_MAIN, MAX_CON_LIMIT, EMPTY, EMPTY );
			continue;
		}

		clientHandler[i].portNo = talkPort;
		log_write( LOG_MAIN, NEW_CONNECTION, EMPTY, EMPTY );
		pthread_create( &clientHandler[i].thread, NULL,
			( void * )threadHandler, ( void * )&i );
		if( pthread_detach( clientHandler[i].thread ) ){
			fprintf( stderr, "Error detaching thread %d\n", i );
			exit( 1 );
		}
	}

	close_socket( listenFD );
	return 0;
}

int getFirstUnusedRecord( TR *r ){
	int i = 0;
	while( i < MAXCON ){
		if( !r[i].used ){
			return i;
		}
		i++;
	}
	return -1;
}

/* make the pthread_t available again */
void cleanupHandler( void *v ){
	int i = *( int * )v;

	pthread_mutex_lock( &threadMutex );
	clientHandler[i].used = false;
	pthread_mutex_unlock( &threadMutex );
}

/* a thread starts from here */
void *threadHandler( void *v ){
	int i = *( int * )v;

	pthread_cleanup_push( cleanupHandler, v );
	pop_protocol( clientHandler[i].portNo );
	pthread_exit( 0 );
	pthread_cleanup_pop( 0 );
}

/* utility! I want to modify header files!!!!!!!!! */
void * smalloc( size_t size ){
	pthread_mutex_lock( &mallocMutex );
	void *space = malloc( size );

	if( space == NULL ){
		fprintf( stderr, "Error allocating memory\n" );
		exit( 1 );
	}

	pthread_mutex_unlock( &mallocMutex );
	return space;
}
