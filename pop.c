#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <pthread.h>

#include "logging.h"
#include "maildir.h"
#include "pop.h"
#include "socket.h"

#define maxUserLen 50
#define maxPassLen 50
#define BUFLEN 512
#define COMMAND_MIN 3 // minimum command length
#define ENDING_LEN 4 // ending: .\r\n\0 - length is 4
#define REVERSE_MASK 0xffffffff
#define ENDING ".\r\n"
#define SPACE 1

// logging
#define LOG_POP "POP"
#define USER "USER"
#define PASS "PASS"
#define QUIT "QUIT"
#define STAT "STAT"
#define LIST "LIST"
#define RETR "RETR"
#define DELE "DELE"
#define NOOP "NOOP"
#define RSET "RSET"
#define NO_AUTH "No Auth"
#define AUTHENTICATED "Authenticated"
#define AUTH_FAILED "Auth failed"
#define TRANSAC_STATE "TRANSACTION STATE"
#define UPDATE_STATE "UPDATE STATE"
#define CLOSING_CONNECTION "Closing Connection"

// OK
#define OK "+OK"
#define OKK "+OK\r\n"
#define GREETINGS "Welcome!"
#define GOT_USER "got username"
#define AUTH_SUCCESS "authenticated ok"
#define LISTING "listing email(s)"

// ERR
#define ERR "+ERR"
#define FAILED_AUTH "Authentication failed, please try again"
#define UNKNOWN "Not expecting this command"
#define LACK "Expecting `command argument`, something is missing"
#define INDEXERR "Index not exists"

/* from main.c */
extern char *EMPTY;

typedef enum state_t State; // this type is for tracking user state
enum state_t { Authorization, Transaction, Update, Abort };

typedef enum auth_t Auth; // this type is for user logging in
enum auth_t { NeedUsername, NeedPassword };

typedef enum command_t Command; // this type has all the possible commands
enum command_t { User, Pass, Quit, Stat, List, Retr, Dele, Noop, Rset, None };

/* from logging.c */
void chopNewline( char * );

/* from main.c */
void *smalloc( size_t );

static void getInput( int, char * );
static Command extractCommand( char * );
static int getOffset( Command );
static bool goodLength( int, char *, int );
static bool goodIndex( int, char *, FilesStruct *, int );
static void authorize( int, State *, char *, char * );
static void transact( int, State *, char *, char * );
static void update( int, FilesStruct *, char *, char * );
static int sumSize( FilesStruct * );
static int countFiles( FilesStruct * );
static void listAll( int, FilesStruct *, char * );
static void markDelete( int, FilesStruct *, int, char * );
static void markReset( FilesStruct * );
static bool isDeleted( FilesStruct *, int );
static void sendFile( int, char *, char *, int );
static int readLine( char *, char * );
static void byteStuff( char *, int );
static char * concat( char *, char *, char * );

/*
* This function is the starting point of the POP protocol
* on the given connection.
*/
void pop_protocol( int fd ){
	char *buffer = smalloc( sizeof( char ) * BUFLEN );
	char user[maxUserLen];

	socket_write( fd, concat( buffer, OK, GREETINGS ) );

	State state = Authorization;
	authorize( fd, &state, user, buffer );
	if( state == Transaction ){
		transact( fd, &state, user, buffer );
	}
	/* update state is called in transact code */
	free( buffer );
}

/*
* authorization uses two substates
* NeedUsername: accpet <user> command
* NeedPassword: accpet <pass> command
* It does not redirect to other state, e.g. transaction
* It only changes the state variable
*/
void authorize( int fd, State *state, char *user, char *buffer ){
	Auth auth = NeedUsername;
	char username[maxUserLen], password[maxPassLen];
	int offset;
	Command command;

	while( *state == Authorization ){
		/* check command format */
		getInput( fd, buffer );
		command = extractCommand( buffer );
		offset = getOffset( command );
		if( !goodLength( fd, buffer, offset ) ){
			continue;
		}

		if( auth == NeedPassword && command == Pass ){
			snprintf( password, maxPassLen, "%s", buffer + offset );
			log_write( LOG_POP, NO_AUTH, PASS, password );
			if( check_user( username, password ) ){
				strcpy( user, username );
				socket_write( fd, concat( buffer, OK, AUTH_SUCCESS ) );
				log_write( LOG_POP, username, AUTHENTICATED, EMPTY );
				log_write( LOG_POP, username, TRANSAC_STATE, EMPTY );
				*state = Transaction;
			}else{
				socket_write( fd, concat( buffer, ERR, FAILED_AUTH ) );
				log_write( LOG_POP, NO_AUTH, AUTH_FAILED, EMPTY );
				auth = NeedUsername;
				continue;
			}
		}else if( auth == NeedUsername && command == User ){
			snprintf( username, maxUserLen, "%s", buffer + offset );
			log_write( LOG_POP, NO_AUTH, USER, username );
			socket_write( fd, concat( buffer, OK, GOT_USER ) );
			auth = NeedPassword;
		}else if( command == Quit ){
			log_write( LOG_POP, NO_AUTH, QUIT, EMPTY );
			*state = Abort;
			close_connection( fd );
			break;
		}else{
			socket_write( fd, concat( buffer, ERR, UNKNOWN ) );
		}
	}
}

/*
* No sub state is needed for transaction state
* transact handles <list>, <stat>, <noop>, <dele>, <retr>, <rset>, <quit>
* transact will call update, when user gives <quit> command
*/
void transact( int fd, State *state, char *user, char *buffer ){
	Command command;
	int offset, index;
	char *file;
	FilesStruct *files = dir_get_list( user );

	while( *state == Transaction ){
		/* check command format */
		getInput( fd, buffer );
		command = extractCommand( buffer );
		offset = getOffset( command );
		if( !goodLength( fd, buffer, offset ) ){
			continue;
		}

		switch( command ){

		case List: /* may have one arg */
			/* does it have an argument? no? */
			if( strlen( buffer ) == strlen( LIST ) ||
					( strlen( buffer ) == strlen( LIST ) + SPACE &&
					buffer[strlen( LIST )] == ' ' ) ){
				listAll( fd, files, buffer );
				log_write( LOG_POP, user, LIST, EMPTY );
			/* it does? get the argument and work with it */
			}else{
				sscanf( buffer + strlen( LIST ) + SPACE, "%d", &index );
				if( !goodIndex( fd, buffer, files, index ) ){
					continue;
				}
				socket_write( fd, concat( buffer, OK, LISTING ) );
				sprintf( buffer, "%d %d\r\n", index, files->FileSize[index - 1] );
				socket_write( fd, buffer );
				socket_write( fd, ENDING );
				sprintf( buffer, "%d", index );
				log_write( LOG_POP, user, LIST, buffer );
			}
			break;

		case Stat:
			sprintf( buffer, "%s %d %d\r\n",
				OK, countFiles( files ), sumSize( files ) );
			socket_write( fd, buffer );
			log_write( LOG_POP, user, STAT, EMPTY );
			break;

		case Noop:
			sprintf( buffer, "%s\r\n", OK );
			socket_write( fd, buffer );
			log_write( LOG_POP, user, NOOP, EMPTY );
			break;

		case Retr: /* need arg */
			sscanf( buffer + offset, "%d", &index );
			if( !goodIndex( fd, buffer, files, index ) ){
				continue;
			}
			sprintf( buffer, "%s", files->FileNames[index - 1] );
			file = get_file( user, buffer );
			sendFile( fd, buffer, file, strlen( file ) );
			sprintf( buffer, "%d", index );
			log_write( LOG_POP, user, RETR, buffer );
			break;

		case Dele: /* need arg */
			sscanf( buffer + offset, "%d", &index );
			if( !goodIndex( fd, buffer, files, index ) ){
				continue;
			}
			markDelete( fd, files, index, buffer );
			log_write( LOG_POP, user, DELE, EMPTY );
			break;

		case Rset:
			markReset( files );
			sprintf( buffer, "%s\r\n", OK );
			socket_write( fd, buffer );
			log_write( LOG_POP, user, RSET, EMPTY );
			break;

		case Quit:
			*state = Update;
			update( fd, files, user, buffer );
			log_write( LOG_POP, user, QUIT, EMPTY );
			break;

		default:
			socket_write( fd, concat( buffer, ERR, UNKNOWN ) );
		}
	}
}

/*
* update handles memory cleaning up
* update deletes emails that have been marked to delete
* update closes the connection
*/
void update( int fd, FilesStruct *files, char *user, char *buffer ){
	int i;
	for( i = 0; i < files->count; i++ ){
		if( isDeleted( files, i ) ){
			delete_mail( user, files->FileNames[i] );
		}
		free( files->FileNames[i] );
	}
	free( files->FileNames );
	free( files->FileSize );
	free( files );
	sprintf( buffer, "%s\r\n", OK );
	socket_write( fd, buffer );
	log_write( LOG_POP, user, CLOSING_CONNECTION, EMPTY );
	close_connection( fd );
}

/*
* because we are not allowed to modify header files,
* we can't pass a buffer pointer to the socket function
* we never need any newline from input
* it is chopped here
*/
void getInput( int fd, char *buffer ){
	char *input;

	input = socket_get_line( fd );
	sprintf( buffer, "%s", input );
	chopNewline( buffer );

	free( input );
}

/*
* extracts the command from the input
*/
Command extractCommand( char *buffer ){
	if( !strncasecmp( buffer, USER, 4 ) ){
		return User;
	}else if( !strncasecmp( buffer, PASS, 4 ) ){
		return Pass;
	}else if( !strncasecmp( buffer, QUIT, 4 ) ){
		return Quit;
	}else if( !strncasecmp( buffer, STAT, 4 ) ){
		return Stat;
	}else if( !strncasecmp( buffer, LIST, 4 ) ){
		return List;
	}else if( !strncasecmp( buffer, RETR, 4 ) ){
		return Retr;
	}else if( !strncasecmp( buffer, DELE, 4 ) ){
		return Dele;
	}else if( !strncasecmp( buffer, NOOP, 4 ) ){
		return Noop;
	}else if( !strncasecmp( buffer, RSET, 4 ) ){
		return Rset;
	}else{
		return None;
	}
}

/*
* offset is the offset of the argument in the input
*/
int getOffset( Command command ){
	switch( command ){

	case User:
	case Pass:
	case Retr:
	case Dele:
		return 5;

	default:
		return 0;
	}
}

/*
* well, we don't need excessive error checking
* but I still find it helpful to have SOME err checking
*/
bool goodLength( int fd, char *buffer, int offset ){
	if( ( int )strlen( buffer ) <= offset ){
		socket_write( fd, concat( buffer, ERR, LACK ) );
		return false;
	}
	return true;
}

/*
* this is not error chcking
* this is to determine if there's a file
* with the given index, in the file structure
* it ignores the files that are marked to be deleted
*/
bool goodIndex( int fd, char *buffer, FilesStruct *files, int index ){
	if( index > files->count || index <= 0
			|| isDeleted( files, index - 1 ) ){
		socket_write( fd, concat( buffer, ERR, INDEXERR ) );
		return false;
	}
	return true;
}

/* returns the total size of the files that are not marked to delete */
int sumSize( FilesStruct *files ){
	int i, sum = 0;
	for( i = 0; i < files->count; i++ ){
		if( isDeleted( files, i ) ){
			continue;
		}
		sum += files->FileSize[i];
	}
	return sum;
}

/* count how many emails are in the folder, and are not marked to deleted */
int countFiles( FilesStruct *files ){
	int i, count = 0;
	for( i = 0; i < files->count; i++ ){
		if( files->FileSize[i] >= 0 ){
			count++;
		}
	}
	return count;
}

/* this is used for <list> command with no arguments */
void listAll( int fd, FilesStruct *files, char *buffer ){
	int i, n;
	socket_write( fd, concat( buffer, OK, LISTING ) );
	for( i = 0, n = 1; i < files->count; i++, n++ ){
		if( isDeleted( files, i ) ){
			continue;
		}
		sprintf( buffer, "%d %d\r\n", n, files->FileSize[i] );
		socket_write( fd, buffer );
	}
	socket_write( fd, ENDING );
}

/*
* "marking one email to be deleted"
* what is actually done is the file size of the email
* is flipped. a binary xor with -1 is used.
* any email cannot be of negative size.
* so by applying xor, the size will be negative.
* and negative size is used to indicate the email is marked
* as to be deleted
*/
void markDelete( int fd, FilesStruct *files, int index, char *buffer ){
	if( index > 0 && index <= files->count ){
		files->FileSize[index - 1] ^= REVERSE_MASK;
		socket_write( fd, OKK );
	}else{
		socket_write( fd, concat( buffer, ERR, INDEXERR ) );
	}
}

/* so if an email needs to be deleted, its size looks negative */
bool isDeleted( FilesStruct *files, int index ){
	return files->FileSize[index] < 0;
}

/* reset the mark, just apply xor -1 again on the files with negative size */
void markReset( FilesStruct *files ){
	int i;
	for( i = 0; i < files->count; i++ ){
		if( isDeleted( files, i ) ){
			files->FileSize[i] ^= REVERSE_MASK;
		}
	}
}

/* this function takes care of byte-stuffing for every line */
void sendFile( int fd, char *buffer, char *file, int flen ){
	int index = 0, len = 0;
	sprintf( buffer, "%s\r\n", OK );
	socket_write( fd, buffer );
	while( index < flen ){
		len = readLine( file + index, buffer );
		index += len;
		byteStuff( buffer, len );
		socket_write( fd, buffer );
	}
	socket_write( fd , ENDING );
	free( file );
}

/* read one line from a char* to buffer */
int readLine( char *file, char *buffer ){
	int len = 0;
	/* need to leave space for \n\0 and potential byte-stuffying */
	while( file[len] != '\n' && len < BUFLEN - SPACE * 2 ){
		buffer[len] = file[len];
		len++;
	}
	buffer[len++] = '\n';
	buffer[len] = '\0';
	return len;
}

void byteStuff( char *line, int len ){
	int i;
	if( line[0] == '.' ){
		/* start from len + 1 because we want \0 to be copied as well */
		for( i = len + 1; i > 0; i-- ){
			line[i] = line[i-1];
		}
	}
}

/* utility function ! */
char* concat( char *buffer, char *c1, char *c2 ){
	sprintf( buffer, "%s %s\r\n", c1, c2 );
	return buffer;
}
