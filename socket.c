#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "socket.h"

#define BUFLEN 512
#define PORTBUFF 10

static int socketFD, newSocketFD;
static unsigned int cliLen;
static struct sockaddr_in serverAddress, clientAddress;

/* from main.c */
void * smalloc( size_t );

/*
* Creates a new socket and returns the socket descriptor.
*/
int socket_setup( int portno ){

	socketFD = socket( AF_INET, SOCK_STREAM, 0 );

	if ( socketFD < 0 ) {
		perror( "ERROR opening socket" );
		exit( 1 );
	}

	bzero( ( void * ) &serverAddress, sizeof( serverAddress ) );

	/* Create address we're going to listen on (given port number)
	 - converted to network byte order & any IP address for
	 this machine */
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons( portno );  // store in machine-neutral format

	/* Bind address to the socket */
	if ( bind( socketFD, ( struct sockaddr * ) &serverAddress,
			sizeof( serverAddress ) ) < 0 ) {
		perror( "ERROR on binding" );
		exit( 1 );
	}

	cliLen = sizeof( clientAddress );

	/* Listen on socket - means we're ready to accept connections -
	 incoming connection requests will be queued */
	listen( socketFD, PORTBUFF );

	return socketFD;
}

/*
* Listens for a new connection,
* once there is a new connection
* the file descriptor of the new open connection is returned.
*/
int socket_get_new_connection( int sockfd ){
	/* Accept a connection - block until a connection is ready to
	 be accepted. Get back a new file descriptor to communicate on. */
	newSocketFD = accept( sockfd, ( struct sockaddr * ) &clientAddress,
						&cliLen );
	if ( newSocketFD < 0 ) {
		perror( "ERROR on accept" );
		exit( 1 );
	}

	return newSocketFD;
}

/*
* Reads a 'line' from the given connection.
* A 'line' means a sequence of characters ending with a CRLF pair.
* The string being returned must include the CRLF pair.
*/
char* socket_get_line( int fd ){
	char *buffer = smalloc( sizeof( char ) * BUFLEN );
	bzero( ( void * )buffer, sizeof( buffer ) );

	int n = read( fd, buffer, BUFLEN );
	if ( n < 0 ) {
		perror( "ERROR reading from socket" );
		exit( 1 );
	}

	return buffer;
}


/*
* Writes the given string to the given connection descriptor.
*/
void socket_write( int fd, char* message ){
	int n = -1, len;

	len = strlen( message );

	if( len <= BUFLEN ){
		n = write( fd, message, len );
	}

	if ( n < 0 ) {
		perror( "ERROR writing to socket" );
		exit( 1 );
	}
}


/*
* Closes the given connection descriptor.
*/
void close_connection( int fd ){
	close( fd );
}

/*
* Closes the given socket.
*/
void close_socket( int fd ){
	close( fd );
}

