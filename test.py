import poplib
import sys
import threading
import os
import time
from random import choice, randint
from uuid import uuid4

IOLock = threading.Lock()

class PopClient(threading.Thread):
	def __init__(self, u, p, port):
		super(PopClient, self).__init__()
		self._user = u
		self._pass = p
		self.port = port

	def run(self):
		time.sleep(randint(0,5))
		M = poplib.POP3('localhost', self.port)
		# M = poplib.POP3('lister.csse.unimelb.edu.au', self.port)

		time.sleep(randint(1,3))
		M.user(self._user)

		time.sleep(randint(1,3))
		M.pass_(self._pass)

		IOLock.acquire()
		wel = M.getwelcome()
		print wel
		IOLock.release()

		time.sleep(randint(1,3))
		IOLock.acquire()
		s = M.stat()
		print s
		IOLock.release()

		time.sleep(randint(1,3))
		IOLock.acquire()
		l = M.list()
		print l
		IOLock.release()

		IOLock.acquire()
		r = M.retr(randint(1, len(l[1])))
		print '\n'.join(r[1][:5])
		print
		IOLock.release()

		time.sleep(randint(1,3))
		IOLock.acquire()
		M.quit()
		IOLock.release()

def randChar():
	l = [chr(x) for x in range(ord('a'), ord('z') + 1)]
	l += [chr(x) for x in range(ord('A'), ord('Z') + 1)]
	l += [' ', ',', '.', '!', '?', ':']
	while 1:
		yield choice(l)

charGen = randChar()

def randLine():
	line = []
	for j in xrange(0, randint(30, 80)):
		line.append(charGen.next())
	return ''.join(line)

def makeLines(x):
	l = []
	for i in xrange(x):
		l.append(randLine())
	return "\r\n".join(l) + "\r\n"

def test(port=110):
	if not os.path.exists("./maildir"):
		print "Need to run setup first"
		exit(1)

	clients = []
	for user in os.listdir("./maildir"):
		with open("./maildir/" + user + "/pass", "r") as p:
			clients.append(PopClient(user, p.readline().strip(), port))

	print "Watch your server shell"
	for c in clients:
		c.start()
	for c in clients:
		c.join()

def setup(num=30):
	if not os.path.exists("./maildir"):
		os.mkdir("maildir")
	num = num - len(os.listdir("./maildir"))
	if num <= 0:
		print "Already have this many users"
		exit(0)
	for i in xrange(num):
		randhex = str(uuid4().get_hex())
		path = "./maildir/" + randhex[:12]
		os.mkdir(path)
		with open(path + "/pass", "w") as f:
			f.write(randhex[:20])
		for j in xrange(randint(1, 4)):
			with open(path + "/" + uuid4().get_hex()[:12], "w") as f:
				f.write(makeLines(randint(40, 120)))

def fresh():
	num = int(raw_input("Number of users: "))
	setup(num)
	raw_input("Start your server in another shell.\n"
		+ "e.g. pop3 1234 maildir log.txt\n"
		+ "Press any key when you are done...")
	port = int(raw_input("Port Number: "))
	test(port)

def main():
	if len(sys.argv) >= 2:
		if sys.argv[1] == 'fresh':
			fresh()

		elif sys.argv[1] == 'test':
			try:
				port = int(sys.argv[2])
				test(port)
			except (ValueError, IndexError):
				test()

		elif sys.argv[1] == 'setup':
			try:
				num = int(sys.argv[2])
				setup(num)
			except (ValueError, IndexError):
				setup()
	else:
		print "Usage: python ./%s fresh | test [port] | setup [n]" %sys.argv[0]

if __name__ == '__main__':
	main()
